Agenda :

Lession-3 : Designing highly available, cost-efficient, fault-tolerant, scalable systems [Completed]

- CloudWatch   [Completed]
- CloudTrail   [Completed]
- Config       [Completed]

Lession-6 : EC2 [Completed] 
 - Lambada                  [Larners want to recap ]


Lession-10 : Application Services [Pending]

Lession-11 : Security Practices for Optimum Cloud Deployment 

Lession-12 : Disater Recovery 

Lession-13 : Troubleshooting 

Lession-14 : Exam [Completed]


White Papers :
https://aws.amazon.com/whitepapers/?whitepapers-main.sort-by=item.additionalFields.sortDate&whitepapers-main.sort-order=desc&awsf.whitepapers-flag=flag%23new&awsf.whitepapers-content-type=content-type%23whitepaper&awsf.whitepapers-content-category=*all&awsf.whitepapers-category=categories%23network

https://aws.amazon.com/certification/certified-solutions-architect-associate/

https://www.amazon.com/Certified-Solutions-Architect-Associate-Training-ebook/dp/B07PND36WD/ref=sr_1_1?dchild=1&keywords=aws+csa+associate+exam+dump&qid=1594177746&sr=8-1

https://www.amazon.com/Certified-Solutions-Architect-Official-Study-ebook/dp/B01M6W6WYD/ref=sr_1_2?dchild=1&keywords=aws+csa+associate+exam+dump&qid=1594177746&sr=8-2

https://aws.amazon.com/certification/certification-prep/
