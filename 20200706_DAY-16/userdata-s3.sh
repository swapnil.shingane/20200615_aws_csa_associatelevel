#!/bin/bash 
# Our OS is Ubutnu and Update the Local Repository
sudo apt-get update 

# Install Utility Softwares
sudo apt-get install vim curl wget unzip elinks tree git -y 

# Download, Install & Configure WebServer i.e. Apache - apache2 
sudo apt-get install apache2 -y 

# Enable the Daemon / Service at Boot Level 
sudo systemctl enable apache2.service 

# Start the Daemon / Service 
sudo systemctl start apache2.service 

# Delete default index.html 
sudo rm -rf /var/www/html/index.html 

# Navigate to DocumentRoot 
cd /var/www/html/

# Update the Repository 
sudo apt-get update

# Download, & Install aws cli 
sudo apt-get install awscli -y

# Download Website Code From S3 bucket 
# sudo aws s3 cp s3://my_bucket/my_folder/my_file.ext my_copied_file.ext
aws s3 cp --recursive s3://learnbydoing/index.html index.html

# Sync the Data from s3 to EC2 instance 

sudo aws s3 sync s3://learnbydoing/ /var/www/html/ 

# Restart the Daemon / Service 
sudo systemctl restart apache2.service